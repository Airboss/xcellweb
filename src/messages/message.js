export class MessageItem {
  constructor(type, title, message) {
    this.type = type;
    this.title = title;
    this.message = message;
  }
}

export class MessageType {
  static TypeEnum = {
    Success: 1,
    Warning: 2,
    Error: 3,
    properties: {
      1: {name: "success", value: 1, code: "S"},
      2: {name: "warning", value: 2, code: "W"},
      3: {name: "error", value: 3, code: "E"}
    }
  };
}

