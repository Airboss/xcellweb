import {Dispatcher, handle} from 'aurelia-flux';
import {MessageItem, MessageType} from 'messages/message';
import * as toastr from "toastr";

export class MessageService {
  static inject = [Dispatcher];

  constructor(dispatcher) {
    this.dispatcher = dispatcher;
  };

  @handle("msg.post")
  messagePost(action, payload){
    switch(payload.type) {
      case MessageType.TypeEnum.Success:
        toastr.success(payload.message, payload.title);
        break;
      case MessageType.TypeEnum.Warning:
        toastr.warning(payload.message, payload.title);
        break;
      case MessageType.TypeEnum.Error:
        toastr.error(payload.message, payload.title);
        break;
    }
  }
}