import {MessageItem, MessageType} from 'messages/message';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store';
import {MultiObserver} from 'resources/multi-observer';
import {computedFrom} from 'aurelia-framework';
import moment from 'moment';

export class Leg {
  constructor(multiObserver, dispatcher, options)
  {
    this.multiObserver = multiObserver;
    this.dispatcher = dispatcher;

    options = options || {
          tas: "",
          tc: "",
          winds: "",
          wca: "",
          mvar: "",
          gs: "",
          dist: "",
          flttime: ""
        };

    this.tas = options.tas;
    this.tc = options.tc;
    this.winds = options.winds;
    this.wca = options.wca;
    this.mvar = options.mvar;
    this.gs = options.gs;
    this.dist = options.dist;
    this.flttime = options.flttime;

    //this.calcFltTime();
    this.calcMagHeading();

    //this.multiObserver.observe([[this, 'gs'], [this, 'dist']], this.calcFltTime.bind(this));
    this.multiObserver.observe([[this, 'dist'],[this, 'flttime']], this.calcFltTime.bind(this));
    this.multiObserver.observe([[this, 'tc'], [this, 'wca'], [this, 'mvar']], this.calcMagHeading.bind(this));
  }

  getStoreData() {
    let data = [this.tas, this.tc, this.winds, this.wca, this.mvar, this.gs, this.dist, this.flttime];
    return data;
  }

  calcMagHeading() {
    var hdg = parseInt(this.tc) + parseInt(this.wca) + parseInt(this.mvar);

    if(hdg < 0)
      hdg += 360;

    if(hdg > 359)
      hdg = hdg % 360;

    this.mh = hdg;

    return hdg;
  }

  calcFltTime() {
    //var seconds = 0;
    //if(parseFloat(this.gs) !== 0.0)
    //  seconds = parseInt((parseFloat(this.dist) / parseFloat(this.gs)) * 3600);
    //
    //this.flttime = seconds;

    this.dispatcher.dispatch('fltlog.leg.update', undefined);
  }
}
