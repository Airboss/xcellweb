import {FlightLogStore} from 'flight-log/flight-log.store';
import {Dispatcher} from 'aurelia-flux'

export class FlightLog {
  static inject = [Dispatcher, FlightLogStore];

  constructor(dispatcher, flightLogStore) {
    this.dispatcher = dispatcher;
    this.flightLogStore = flightLogStore;
  }

  deactivate() {
    this.dispatcher.dispatch('currSet.save', undefined);
  }

  selectFlight(index) {
    this.flightLogStore.selectFlight(index);
    this.dispatcher.dispatch('currSet.save', undefined);
  }
}