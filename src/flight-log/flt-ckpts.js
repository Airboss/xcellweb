import {customElement} from 'aurelia-framework';
import {FlightLogStore} from 'flight-log/flight-log.store';
import {DialogService} from 'aurelia-dialog';
import {MaskedPadDialog} from 'resources/dialogs/masked-pad-dialog'
import moment from 'moment';

@customElement("flt-ckpts")
export class FltCkpts {
  static inject = [DialogService, FlightLogStore];

  constructor(dialogService, flightLogStore) {
    this.dialogService = dialogService;
    this.flightLogStore = flightLogStore;

    this.myTimer;
    this.updateTimer();

    this.lockCheckpointTime = [];
  }

  attached(params, queryString, routeConfig)
  {
    this.myTimer = setInterval(()=> {
      this.updateTimer();
    }, 1000);

    if(this.flightLogStore.activeFlight.startTime !== "")
      this.lockStartTime = true;

    if(this.flightLogStore.activeFlight.endTime !== "")
      this.lockEndTime = true;

    for(let i=0, ii=this.flightLogStore.activeFlight.checkPoints.length; i < ii; i++) {
      this.lockCheckpointTime.push(this.flightLogStore.activeFlight.checkPoints[i].clockActual !== "" ? true : false);
    }
  }

  openMaskedPad(config) {
    if(config == undefined)
      return;

    this.dialogService.open({ viewModel: MaskedPadDialog, model: config}).then((result) => {});
  }

  toHHMM(value) {
    if(value == undefined || value == "")
      return "";

    let v = value.padZero(4);
    let v1 = v.substr(0,2);
    let v2 = v.substr(2,2);

    return v1 + ":" + v2;
  }

  fromHHMM(value) {
    if(value == undefined)
      return value;

    return value.replace(':','');
  }

  updateTimer()
  {
    this.currentGMT = moment().utc(); //.format('HH:mm:ss');
    this.currentLCL = moment(); //.format('h:mm:ss A');
    this.currentOffset = moment().format('Z');
  }

  checkFourDigits(event) {
    let v = event.target.value;
    let r = v.padZero(4);
    event.target.value = r;
  }

  addItem() {
    this.flightLogStore.activeFlight.addCheckPoint();
    this.lockCheckpointTime.push(false);
  }

  removeItem() {
    this.flightLogStore.activeFlight.removeCheckPoint();
    this.lockCheckpointTime.pop();
  }

  toggleCheckpointEditLock() {
    this.flightLogStore.activeFlight.checkpointEditLocked = !this.flightLogStore.activeFlight.checkpointEditLocked;
  }

  setStartTime()
  {
    if(this.lockStartTime) return;

    this.flightLogStore.activeFlight.startTime = this.currentLCL.format('HH:mm');
    this.lockStartTime = true;
  }

  toggleLockStartTime() {
    this.lockStartTime = ! this.lockStartTime;
  }

  toggleLockEndTime() {
    this.lockEndTime = ! this.lockEndTime;
  }

  setEndTime() {
    if(this.lockEndTime) return;

    this.flightLogStore.activeFlight.endTime = this.currentLCL.format('HH:mm');
    this.lockEndTime = true;
  }

  setCheckPointTime(index) {
    if(this.lockCheckpointTime[index]) return;

    this.flightLogStore.activeFlight.checkPoints[index].clockActual = this.currentLCL.format('HH:mm');
    this.lockCheckpointTime[index] = true;
  }

  toggleLockCheckpointTime(index) {
    this.lockCheckpointTime[index] = !this.lockCheckpointTime[index];
  }

  //resetCheckpoints() {
  //  let params = {title: 'Confirm Reset', body: 'Are you sure?', buttonOk: 'Yes', buttonCancel: 'No'};
  //  this.dialogService.open({ viewModel: confirmDialog, model: params}).then((result) => {
  //    if (!result.wasCancelled) {
  //      this.flightLogStore.activeFlight.resetCheckpoints();
  //
  //      this.flightLogStore.activeFlight.startTime = "";
  //      this.lockStartTime = false;
  //      this.flightLogStore.activeFlight.endTime = "";
  //      this.lockEndTime = false;
  //      this.flightLogStore.activeFlight.timeToDescend = "";
  //
  //
  //      for(let i=0, ii=this.checkPoints.length;i<ii;i++) {
  //        this.lockCheckpointTime[i] = false;
  //      }
  //    }
  //  });
  //}
}