import {Dispatcher, handle} from 'aurelia-flux';
import {MessageItem, MessageType} from 'messages/message';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store';
import {MultiObserver} from 'resources/multi-observer';
import {computedFrom} from 'aurelia-framework';
import moment from 'moment';
import {Leg}  from './leg';
import {CheckPoint}  from './check-point';

export class Flight {
  constructor(multiObserver, aircraftData, dispatcher) {
    this.multiObserver = multiObserver;
    this.aircraftData = aircraftData;
    this.dispatcher = dispatcher;

    this.to = "";
    this.from = "";
    this.alt = "";
    this.fuelEnroute = "";
    this.fuelReserve = "";
    this.fuelClimb = "";
    this.fuelTotal = 0;
    this.distTotal = 0;
    this.timeTotal = 0;

    this.startTime = "";
    this.endDist = "";
    this.endET = "";
    this.endETA = ""
    this.timeToDescend = "";
    this.minutesToDescend = "";
    this.endTime = "";
    this.totalFlightTime = "";

    this.legEditLocked = false;
    this.checkpointEditLocked = false;

    this.legs = [];
    this.checkPoints = [];

    this.multiObserver.observe([[this, 'endET'],[this, 'endTime'],[this, 'minutesToDescend']], ()=>{this.updateEstimateTimes()});
    this.multiObserver.observe([[this, 'fuelEnroute'], [this, 'fuelReserve'], [this, 'fuelClimb']], ()=>{this.calcFuelTotal()});
    this.multiObserver.observe([[this, 'startTime'], [this, 'endTime']], ()=>{this.updateCheckpoints(); this.calcTotalFlightTime()});
  }

  getStoreData() {
    let data = [this.to, this.from,
                this.fuelEnroute, this.fuelReserve, this.fuelClimb,
                this.startTime, this.endDist, this.endET, this.endETA, this.minutesToDescend, this.endTime, this.totalFlightTime,
                this.alt];

    return data;
  }

  loadStoreData(data) {
    this.to = data[0];
    this.from = data[1];
    this.fuelEnroute = data[2];
    this.fuelReserve = data[3];
    this.fuelClimb = data[4];

    this.startTime = data[5];
    this.endDist = data[6];
    this.endET = data[7];
    this.endETA = data[8];
    this.minutesToDescend = data[9];
    this.endTime = data[10];
    this.totalFlightTime = data[11];
    this.alt = data[12];

    this.calcTotals();
    this.calcFuelTotal();
    this.calcTotalFlightTime();
    this.updateEstimateTimes();
  }

  addLeg() {
    let leg = new Leg(this.multiObserver, this.dispatcher);
    this.legs.push(leg);
  }

  removeLeg() {
    if(this.legs.length > 0)
      this.legs.pop();
  }

  addCheckPoint() {
    let checkpoint = new CheckPoint(this.multiObserver, this.dispatcher);
    this.checkPoints.push(checkpoint);
  }

  removeCheckPoint() {
    if(this.checkPoints.length > 0)
      this.checkPoints.pop();
  }

  resetCheckpoints() {
    if(this.checkPoints.length > 0){
      this.startTime = "";
      this.endDist = 0;
      this.endET = "";
      this.endETA = "";
      this.minutesToDescend = "";
      this.timeToDescend = "";
      this.endTime = "";

      for(let i=0, ii=this.checkPoints.length;i<ii;i++)
      {
        let item = this.checkPoints[i];
        item.timeEstimate = "";
        item.timeActual = "";
        item.clockEstimate = "";
        item.clockActual = "";
        item.gs = 0;
      }
    }
  }

  updateCheckpoints() {
    this.updateDistances();
    this.updateActualTime();
    this.updateEstimateTimes();
  }

  updateDistances() {
    let currDist = this.distTotal;

    for(let i=0, ii=this.checkPoints.length;i<ii;i++) {
      let item = this.checkPoints[i];
      item.distToGo = currDist-item.dist;
      currDist -= item.dist;
    }

    this.endDist = currDist;
  }

  updateEstimateTimes() {
    if(this.checkPoints.length == 0) return;

    let time = this.startTime;

    for(let i=0, ii=this.checkPoints.length;i<ii;i++) {
      let item = this.checkPoints[i];
      if(i > 0)
        time = this.checkPoints[i-1].clockActual;

      item.clockEstimate = moment(time,'HH:mm').add(item.timeEstimate, 'minutes').format('HH:mm');
    }
    time = this.checkPoints[this.checkPoints.length-1].clockActual;
    this.endETA = moment(time,'HH:mm').add(this.endET, 'minutes').format('HH:mm');
    this.timeToDescend = moment(this.endETA, 'HH:mm').subtract(this.minutesToDescend, 'minutes').format('HH:mm');
  }

  updateActualTime() {
    if(this.checkPoints.length == 0) return;

    let time = this.startTime;

    for(let i=0, ii=this.checkPoints.length;i<ii;i++) {
      let item = this.checkPoints[i];
      if(i > 0)
        time = this.checkPoints[i-1].clockActual;

      let t = moment(item.clockActual,'HH:mm', true);
      if(t.isValid())
        item.timeActual = t.diff(moment(time, 'HH:mm'), 'minutes');
    }
  }

  calcTotalFlightTime() {
    let start = moment(this.startTime,'HH:mm', true);
    let end = moment(this.endTime,'HH:mm', true);
    if(start.isValid() && end.isValid()) {
      let duration = moment.duration(end.diff(start));
      this.totalFlightTime = duration.asSeconds();
    }
    else {
      this.totalFlightTime = "";
    }
  }

  calcTotals() {
    this.distTotal = 0;
    this.timeTotal = 0;

    for(let i=0, ii=this.legs.length; i < ii; i++) {
      this.distTotal += parseInt(this.legs[i].dist);
      this.timeTotal += this.legs[i].flttime;
    }
  }

  calcFuelTotal() {
    this.fuelTotal = parseFloat(this.fuelEnroute) + parseFloat(this.fuelClimb) + parseFloat(this.fuelReserve);
  }
}
