import {Dispatcher, handle} from 'aurelia-flux';
import {MessageItem, MessageType} from 'messages/message';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store';
import {MultiObserver} from 'resources/multi-observer';
import {Flight}  from './flight';
import {Leg}  from './leg';
import {CheckPoint}  from './check-point';

export class FlightLogStore {
  static inject = [MultiObserver, AircraftDataStore, Dispatcher];

  constructor(multiObserver, aircraftData, dispatcher) {
    this.multiObserver = multiObserver;
    this.aircraftData = aircraftData;
    this.dispatcher = dispatcher;

    this.resetCurrent();
  }

  @handle('checkpoint.update')
  updateCheckpoints() {
    for(let i=0;i<this.flights.length;i++) {
      this.flights[i].updateCheckpoints();
    }
  }

  @handle('fltlog.leg.update')
  updateTotals() {
    for(let i=0;i<this.flights.length;i++) {
      this.flights[i].calcTotals();
    }
  }

  @handle('current.reset')
  resetCurrent() {
    this.flights = [
      new Flight(this.multiObserver, this.aircraftData, this.dispatcher),
      new Flight(this.multiObserver, this.aircraftData, this.dispatcher),
      new Flight(this.multiObserver, this.aircraftData, this.dispatcher)];
    this.selectedFlightIndex = 0;
    this.activeFlight = this.flights[this.selectedFlightIndex];
  }

  toJSON() {
    if(this.flights.length <= 0)
      return;

    let data = [];

    for(var j = 0, len = this.flights.length; j < len; j++) {
      let a = this.flights[j].getStoreData();

      let b = [];
      for (let i = 0; i < this.flights[j].legs.length; i++) {
        let t = this.flights[j].legs[i].getStoreData();
        b.push(t);
      }

      let c = [];
      for (let i = 0; i < this.flights[j].checkPoints.length; i++) {
        let t = this.flights[j].checkPoints[i].getStoreData();
        c.push(t);
      }

      let fltData = [a, b, c];
      data.push(fltData);
    }
    return JSON.stringify(data);
  }

  // needs to handle array of flights
  fromJSON(jsonData) {
    //clear for new data
    if(jsonData === 'undefined')
      return;

    if(this.flights.length > 0)
      this.flights = [];

    let arrays = JSON.parse(jsonData);

    for(let j = 0; j < arrays.length; j++) {
      //let data = [this.to, this.from,
      //            this.fuelEnroute, this.fuelReserve, this.fuelClimb,
      //            this.startTime, this.endDist, this.endET, this.endETA, this.minutesToDescend, this.endTime, this.totalFlightTime,
      //            this.alt];
      let flight = new Flight(this.multiObserver, this.aircraftData, this.dispatcher);

      //let data = arrays[j][0];
      flight.loadStoreData(arrays[j][0]);

      for (let i = 0; i < arrays[j][1].length; i++)
      {
        var data = arrays[j][1][i];

        let options = {
          tas: data[0],
          tc: data[1],
          winds: data[2],
          wca: data[3],
          mvar: data[4],
          gs: data[5],
          dist: data[6],
          flttime: data[7]
        };
        let leg = new Leg(this.multiObserver, this.dispatcher, options);
        flight.legs.push(leg);
      }

      for (let i = 0; i < arrays[j][2].length; i++) {
        var data = arrays[j][2][i];

        let options = {
          name: data[0],
          dist: data[1],
          distToGo: data[2],
          timeEstimate: data[3],
          timeActual: data[4],
          clockEstimate: data[5],
          clockActual: data[6],
          gs: data[7]
        };
        let cp = new CheckPoint(this.multiObserver, this.dispatcher, options);

        flight.checkPoints.push(cp);
      }

      this.flights.push(flight);
    }

    //load last so caluclations have data if available
    this.selectFlight(this.selectedFlightIndex);
    this.activeFlight.loadStoreData(arrays[0][0]);
  }

  selectFlight(index) {
    this.selectedFlightIndex = index;
    this.activeFlight = this.flights[this.selectedFlightIndex];
  }
}
