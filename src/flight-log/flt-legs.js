import {customElement, inject, bindable} from 'aurelia-framework';
import {DialogService} from 'aurelia-dialog';
import {FlightLogStore} from 'flight-log/flight-log.store';
import moment from 'moment';
import {MaskedPadDialog} from 'resources/dialogs/masked-pad-dialog'

@customElement("flt-legs")
export class FltLegs {
  static inject = [FlightLogStore, DialogService];

  constructor(flightLogStore, dialogService) {
    this.flightLogStore = flightLogStore;
    this.dialogService = dialogService;
  }

  addItem() {
    this.flightLogStore.activeFlight.addLeg();
  }

  removeItem() {
    this.flightLogStore.activeFlight.removeLeg();
  }

  toggleLegEditLock() {
    this.flightLogStore.activeFlight.legEditLocked = !this.flightLogStore.activeFlight.legEditLocked;
  }

  openMaskedPad(config) {
    if(config == undefined)
      return;

    this.dialogService.open({ viewModel: MaskedPadDialog, model: config}).then((result) => {});
  }

  secondsToHHMM(value){
    if(value === undefined || value === "")
      return value;

    var hours   = Math.floor(value / 3600);
    var minutes = Math.floor((value - (hours * 3600)) / 60);

    return hours.padZero(2) + minutes.padZero(2);
  }

  hhmmToSeconds(value){
    if(value === undefined || value === "")
      return "";

    var secs   = parseInt(value.slice(0, 2)) * 3600;
    secs += parseInt(value.slice(2, 4)) * 60;

    return parseInt(secs);
  }
}
