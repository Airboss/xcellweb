import {Dispatcher, handle} from 'aurelia-flux';
import {MessageItem, MessageType} from 'messages/message';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store';
import {MultiObserver} from 'resources/multi-observer';
import {computedFrom} from 'aurelia-framework';
import moment from 'moment';

export class CheckPoint {
  constructor(multiObserver, dispatcher, options)
  {
    this.multiObserver = multiObserver;
    this.dispatcher = dispatcher;

    options = options || {
          name: "",
          dist: "",
          distToGo: "",
          timeEstimate: "",
          timeActual: "",
          clockEstimate: "",
          clockActual: "",
          gs: ""
        };

    this.name = options.name;
    this.dist = options.dist;
    this.distToGo = options.distToGo;
    this.timeEstimate = options.timeEstimate;
    this.timeActual = options.timeActual;
    this.clockEstimate = options.clockEstimate;
    this.clockActual = options.clockActual;
    this.gs = options.gs;

    this.multiObserver.observe(
        [[this, 'dist'], [this, 'timeEstimate'], [this, 'clockActual']],
        ()=>{this.dispatcher.dispatch('checkpoint.update', undefined);}
    );
  }

  getStoreData() {
    let data = [this.name, this.dist, this.distToGo, this.timeEstimate, this.timeActual, this.clockEstimate, this.clockActual, this.gs];
    return data;
  }
}