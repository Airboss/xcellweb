import {DialogController} from 'aurelia-dialog';
import {computedFrom} from 'aurelia-framework';

export class SaveSetDialog {
  static inject = [DialogController];

  constructor(controller) {
    this.controller = controller;

    this.charCount = 0;
  }

  activate(params) {
    this.params = params;
    this.setName = params.setName;
  }

  save() {
    this.params.setName = this.setName;
    this.controller.ok();
  }

  @computedFrom("setName")
  get canSave() {
    this.charCount = this.params.maxChars - this.setName.length;
    return this.setName.length == 0;
  }
}