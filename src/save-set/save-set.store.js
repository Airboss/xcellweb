import {Dispatcher, handle} from 'aurelia-flux';
import {MessageItem, MessageType} from 'messages/message';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store';
import {WtBalStore} from 'wt-bal/wt-bal.store';
import {PerfStore} from 'perf/perf.store';
import {FlightLogStore} from 'flight-log/flight-log.store';

export class SaveSetStore {
  static inject = [Dispatcher, AircraftDataStore, WtBalStore, PerfStore, FlightLogStore];
  static version = '0';
  static setMax = 10;
  static maxNameLength = 25;

  constructor(dispatcher, aircraftDataStore, wtbalStore, perfStore, flightLogStore) {
    this.dispatcher = dispatcher;
    this.aircraftDataStore = aircraftDataStore;
    this.wtbalStore = wtbalStore;
    this.perfStore = perfStore;
    this.flightLogStore = flightLogStore;

    this.sets = [];

    this.initSets();
  }

  initSets() {
    let setsData = localStorage.getItem("xcellweb-sets");
    if (setsData !== null) {
      this.sets = JSON.parse(setsData);
    }
  }

  @handle("currSet.save")
  saveCurrentSet() {
    let data = this.getCurrentSetStringData();
    localStorage.setItem("xcellweb-currentSet", data);

    let msg = new MessageItem(MessageType.TypeEnum.Success, "Current Set", "Saved!");
    this.dispatcher.dispatch('msg.post', msg);
  }

  saveSet(name) {
    let data = this.getCurrentSetStringData();
    let set = {name: name, data: data};
    this.sets.push(set);

    this.saveSetsLocal();
  }

  deleteSet(index) {
    if (index < 0 || (index > (this.sets.length - 1))) {
      return;
    }

    this.sets.splice(index, 1);

    this.saveSetsLocal();
  }

  saveSetsLocal() {
    let data = JSON.stringify(this.sets);
    localStorage.setItem("xcellweb-sets", data);
  }

  loadEmailSet(jsonData) {
    localStorage.setItem("xcellweb-currentSet", jsonData);
    this.loadCurrentSet();
  }

  loadSet(id) {
    if (this.processSetArray(this.sets[id].data)) {
      let msg = new MessageItem(
          MessageType.TypeEnum.Success,
          "Load Set",
          "Set [" + this.sets[id].name + "] Loaded!");
      this.dispatcher.dispatch('msg.post', msg);
    }
    else {
      let msg = new MessageItem(
          MessageType.TypeEnum.Error,
          "Load Set ",
          "An Error occurred. Set [" + this.sets[id].name + "]  Not Loaded!");
      this.dispatcher.dispatch('msg.post', msg);
    }
  }

  @handle("currSet.load")
  loadCurrentSet() {
    if (this.processSetArray(localStorage.getItem("xcellweb-currentSet"))) {
      let msg = new MessageItem(
          MessageType.TypeEnum.Success,
          "Load Set ",
          "Current Set Loaded!");
      this.dispatcher.dispatch('msg.post', msg);
    }
    else {
      let msg = new MessageItem(
          MessageType.TypeEnum.Error,
          "Load Set ",
          "An Error occurred. Current Set Not Loaded!");
      this.dispatcher.dispatch('msg.post', msg);
    }
  }

  getCurrentSetStringData() {
    return SaveSetStore.version
        + "|" + this.aircraftDataStore.availableAircraft[this.aircraftDataStore.selectedAircraft].reg
        + "|" + this.wtbalStore.toJSON()
        + "|" + this.perfStore.toJSON()
        + "|" + this.flightLogStore.toJSON();
  }

  processSetArray(data) {
    if (data === null) {
      let msg = new MessageItem(
          MessageType.TypeEnum.Error,
          "Load Set",
          "Load set not found!");
      this.dispatcher.dispatch('msg.post', msg);

      return false;
    }

    let setArray = data.split("|");

    if (setArray.length !== 5) {
      return false;
    }

    this.aircraftDataStore.updateAircraftByReg(setArray[1]);
    this.wtbalStore.fromJSON(setArray[2]);
    this.perfStore.fromJSON(setArray[3]);
    this.flightLogStore.fromJSON(setArray[4]);

    return true;
  }
}


