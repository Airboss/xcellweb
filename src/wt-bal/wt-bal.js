import {customElement} from 'aurelia-framework';
import {DialogService} from 'aurelia-dialog';
import {WtBalStore} from './wt-bal.store';
import {WtBalMarker} from './wt-bal-marker';
import {WtBalEditDialog} from './wt-bal-edit-dialog';

@customElement("wt-bal")
export class WtBal {
  static inject = [DialogService, WtBalStore];

  constructor(dialogService, wtBalStore) {
    this.dialogService = dialogService;
    this.wtBalStore = wtBalStore;

    this.showing = false;

    this.item = {};
    this.pilotCopilotMoment = 0;
    this.rearPassengersMoment = 0;
    this.baggageMoment = 0;
    this.fuelStartMoment = 0;
    this.fuelEndMoment = 0;
    this.pinColors = WtBalMarker.pinColors;
  }

  editItem(index) {
    this.index = index;
    this.item = this.wtBalStore.items[index];

    let params = {title: "Leg " + (index+1), aircraft: this.wtBalStore.aircraft, item: this.item};
    this.dialogService.open({viewModel: WtBalEditDialog, model: params});

  }
}
