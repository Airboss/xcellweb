//Note: to = Take Off, ldg = Landing
export class WtBalMarker {
  static pinColors = ['navy', 'purple', 'darkgreen', 'brown', 'magenta'];
  constructor(options) {
    options = options || {
          color: 'gray',
          toTop: 0,
          toLeft: 0,
          ldgTop: 0,
          ldgLeft: 0
        };

    this.showing = false;
    this.color = options.color;

    this.toTop = options.toTop;
    this.toLeft = options.toLeft;
    this.ldgTop = options.ldgTop;
    this.ldgLeft = options.ldgLeft;
  }
}
