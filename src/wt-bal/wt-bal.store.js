import {Dispatcher, handle} from 'aurelia-flux';
import {Message, MessageItem, MessageType} from 'messages/message';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store';
import {MultiObserver} from 'resources/multi-observer';
import {WtBalItem} from 'wt-bal/wt-bal-item';

export class WtBalStore {
  static inject = [MultiObserver, AircraftDataStore, Dispatcher];
  static version = '0';

  constructor(multiObserver, aircraftData, dispatcher) {
    this.multiObserver = multiObserver;
    this.aircraftData = aircraftData;
    this.dispatcher = dispatcher;

    this.resetCurrent();
  }

  get isEmpty() {
    return this.items.length == 0;
  }

  @handle('current.reset')
  resetCurrent(action, param) {
    this.items = [];
  }

  toJSON() {
    let array = [];
    for (let i = 0; i < this.items.length; i++) {
      let a = [this.items[i].pilotCopilotWeight, this.items[i].rearPassengersWeight, this.items[i].baggageWeight, this.items[i].fuelLoaded, this.items[i].fuelUsed]
      array.push(a);
    }

    return JSON.stringify(array);
  }

  fromJSON(jsonData) {
    if(this.items.length > 0)
      this.items = [];

    if(!this.aircraft)
      this.aircraft = this.aircraftData.availableAircraft[this.aircraftData.selectedAircraft];

    let arrays = JSON.parse(jsonData);

    for (let i = 0; i < arrays.length; i++) {
      var data = arrays[i];

      if( data.length !== 5)
        continue;

      let options = {
            pilotCopilotWeight: data[0],
            rearPassengersWeight: data[1],
            baggageWeight: data[2],
            fuelLoaded: data[3],
            fuelUsed: data[4]
          };

      this.addRow(options);
    }
  }

  @handle('config.aircraft')
  doSetAircraft(action, param) {
    this.setAircraft(param);
  }

  setAircraft(aircraft) {
    this.aircraft = aircraft;
    for(let i=0; i < this.items.length; i++){
      let item = this.items[i];

      item.aircraft = aircraft;
      item.doCalculations();
    }
  }

  @handle('wtbal.removeRow')
  removeRow(action, param) {
    if(this.items.length > 0) {
      this.items.pop();

      this.dispatcher.dispatch('wtbalchart.update', undefined);
    }
  }

  @handle('wtbal.addRow')
  doAddRow(action, param) {
    this.addRow();
  }

  addRow(options) {
    if(this.items.length > 4) {
      let msg = new MessageItem(MessageType.TypeEnum.Error, "Weight & Balance", "Only 5 legs can be entered at any time!");
      this.dispatcher.dispatch('msg.post', msg);

      return;
    }

    options = options || {
      pilotCopilotWeight: "",
      rearPassengersWeight: "",
      baggageWeight: "",
      fuelLoaded: "",
      fuelUsed: ""
    };

    let newItem = new WtBalItem(this.multiObserver, this.dispatcher, this.aircraft, options);
    this.items.push(newItem);
  }
}