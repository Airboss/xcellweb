
export class WtBalItem {
  constructor(multiObserver, dispatcher, aircraft, options) {
    this.multiObserver = multiObserver;
    this.dispatcher = dispatcher;
    this.aircraft = aircraft;

    this.pilotCopilotWeight = options.pilotCopilotWeight;
    this.rearPassengersWeight = options.rearPassengersWeight;
    this.baggageWeight = options.baggageWeight;
    this.fuelLoaded = options.fuelLoaded;
    this.fuelUsed = options.fuelUsed;
    this.takeoffCg = 0;
    this.landingCg = 0;

    this.multiObserver.observe(
        [[this, "pilotCopilotWeight"], [this, "rearPassengersWeight"], [this, "baggageWeight"], [this, "fuelLoaded"], [this, "fuelUsed"]],
        ()=>this.doCalculations()
    );

    this.doCalculations();
  }

  doCalculations() {
    this.planeEmptyMoment = Math.round(this.aircraft.planeEmptyWeight * this.aircraft.planeEmptyArm);
    this.pilotCopilotMoment = this.pilotCopilotWeight * this.aircraft.pilotCopilotArm;
    this.rearPassengersMoment = this.rearPassengersWeight * this.aircraft.rearPassengersArm;
    this.baggageMoment =  this.baggageWeight * this.aircraft.rearBaggageArm;

    this.fuelStartWeight = this.fuelLoaded * 6.0;
    this.fuelStartMoment = this.fuelStartWeight * this.aircraft.fuelArm;

    this.fuelEndWeight = Number((this.fuelLoaded - this.fuelUsed)  * 6.0).toFixed(1);
    this.fuelEndMoment = this.fuelEndWeight * this.aircraft.fuelArm;

    this.calcTakeoffWeight();
    this.calcLandingWeight();
    this.checkLimits();

    this.dispatcher.dispatch('wtbalchart.update', undefined);
  }

  calcTakeoffWeight() {
    let planeEmptyMoment = this.aircraft.planeEmptyArm * this.aircraft.planeEmptyWeight;
    let weight = parseInt(this.aircraft.planeEmptyWeight) + parseInt(this.pilotCopilotWeight) + parseInt(this.rearPassengersWeight) + parseInt(this.baggageWeight) + parseInt(this.fuelStartWeight);
    let cg =  planeEmptyMoment + parseInt(this.pilotCopilotMoment) + parseInt(this.rearPassengersMoment) + parseInt(this.baggageMoment) + parseInt(this.fuelStartMoment);

    this.takeoffWeight = weight;
    this.takeoffCg = Number((cg / weight).toFixed(2));
  }

  calcLandingWeight() {
    let planeEmptyMoment = this.aircraft.planeEmptyArm * this.aircraft.planeEmptyWeight;
    let weight = parseInt(this.aircraft.planeEmptyWeight) + parseInt(this.pilotCopilotWeight) + parseInt(this.rearPassengersWeight) + parseInt(this.baggageWeight) + parseInt(this.fuelEndWeight);
    let cg = planeEmptyMoment + parseInt(this.pilotCopilotMoment) + parseInt(this.rearPassengersMoment) + parseInt(this.baggageMoment) + parseInt(this.fuelEndMoment);

    this.landingWeight = weight;
    this.landingCg = Number((cg / weight).toFixed(2));
  }

  checkLimits()
  {
    this.isTakeoffOverGross = this.takeoffWeight > this.aircraft.planeGrossWeight;
    this.isTakeoffOutsideCg = !!(this.takeoffCg > this.aircraft.planeCgRange[1] || this.takeoffCg < this.aircraft.planeCgRange[0]);
    this.isLandingOverGross = this.landingWeight > this.aircraft.planeGrossWeight;
    this.isLandingOutsideCg = !!(this.landingCg > this.aircraft.planeCgRange[1] || this.landingCg < this.aircraft.planeCgRange[0]);
    this.isFuelOutsideCapacity = this.fuelLoaded > this.aircraft.fuelCapacity;
    this.isFuelUsedGreaterThanLoaded = (this.fuelUsed - this.aircraft.fuelCapacity) > 0 || (this.fuelLoaded-this.fuelUsed) < 0;
  }
}
