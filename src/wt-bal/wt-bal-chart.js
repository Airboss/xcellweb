import {Dispatcher, handle} from 'aurelia-flux';
import {customElement} from 'aurelia-framework';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store';
import {WtBalStore} from './wt-bal.store';
import {WtBalMarker} from './wt-bal-marker';
import {MultiObserver} from 'resources/multi-observer';

@customElement("wt-bal-chart")
export class WtBalChart {
  static inject = [WtBalStore, AircraftDataStore, Dispatcher];

  constructor(wtbalstore, aircraftData, dispatcher) {
    this.store = wtbalstore;
    this.aircraftData = aircraftData;
    this.dispatcher = dispatcher;

    this.aircraft = this.aircraftData.availableAircraft[this.aircraftData.selectedAircraft];
    this.markers = [new WtBalMarker(), new WtBalMarker(), new WtBalMarker(), new WtBalMarker(), new WtBalMarker()];

    //update chart to display
    this.cgChart = this.aircraftData.availableAircraft[this.aircraftData.selectedAircraft].cgData[0];
  }

  attached() {
    //update chart to display
    this.cgChart = this.aircraftData.availableAircraft[this.aircraftData.selectedAircraft].cgData[0];

    if(this.store.isEmpty)
      return;

    this.wtBalUpdate();
  }

  @handle('wtbalchart.update')
  wtBalUpdate() {
    //set currently available markers
    this.store.items.forEach(
        (item, i) => { this.calcMarkerPosition(item, this.markers[i], i);}
    );

    //hide unused markers
    if(this.store.items.length < 5) {
      let startIdx = this.store.items.length
      for(;startIdx < 5; startIdx++)
        this.markers[startIdx].showing = false;
    }
  }

  calcMarkerPosition(item, marker, index) {
    let xOffset = this.aircraft.cgData[1];
    let xWidth = this.aircraft.cgData[2];
    let yOffset = this.aircraft.cgData[3];
    let yHeight = this.aircraft.cgData[4];
    let cgMin =  this.aircraft.cgData[5];
    let cgMax =  this.aircraft.cgData[6];
    let emptyWeight = this.aircraft.cgData[7];
    let mtow = this.aircraft.cgData[8];

    let xScalar = xWidth / (cgMax - cgMin);
    let yScalar = yHeight / (mtow - emptyWeight);

    //Take off CG markers
    let tx = xOffset + ((item.takeoffCg - cgMin) * xScalar) + 1;
    let ty = yOffset - ((item.takeoffWeight - emptyWeight)  * yScalar) + 1;


    //Landing CG markers
    let lx = xOffset + ((item.landingCg - cgMin) * xScalar) + 1;
    let ly = yOffset - ((item.landingWeight - emptyWeight)  * yScalar) - 14;

    marker.showing = true;
    marker.color = WtBalMarker.pinColors[index];
    marker.toTop = ty;
    marker.toLeft = tx;
    marker.ldgTop = ly;
    marker.ldgLeft = lx;
  }
}

