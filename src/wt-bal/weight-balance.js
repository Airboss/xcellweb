import {Dispatcher} from 'aurelia-flux';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store.js';

export class WeightBalance {
  static inject = [Dispatcher, AircraftDataStore];

  constructor(dispatcher, aircraftData) {
    this.dispatcher = dispatcher;
    this.aircraft = aircraftData.availableAircraft[aircraftData.selectedAircraft];
  }

  activate() {
    this.dispatcher.dispatch('wtbalchart.update', undefined);
  }

  deactivate() {
    this.dispatcher.dispatch('currSet.save', undefined);
  }

  addItem() {
    this.dispatcher.dispatch('wtbal.addRow', undefined);
  }

  removeItem() {
    this.dispatcher.dispatch('wtbal.removeRow', undefined);
  }
}
