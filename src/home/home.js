import {DialogService} from 'aurelia-dialog';
import {Dispatcher} from 'aurelia-flux';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store.js';
import {MessageItem, MessageType} from 'messages/message';
import {SaveSetStore} from 'save-set/save-set.store.js';
import {ConfirmDialog} from 'resources/dialogs/confirm-dialog';
import {SaveSetDialog} from 'save-set/save-set-dialog';

export class Home {
  static inject = [DialogService, AircraftDataStore, Dispatcher, SaveSetStore];
  static version = '0';
  static descLimit = 25;

  heading = 'Welcome to X-Cell Aviation!';
  showing = false;

  constructor(dialogService, aircraftData, dispatcher, saveSetStore) {
    this.dialogService = dialogService;
    this.aircraftData = aircraftData;
    this.dispatcher = dispatcher;
    this.store = saveSetStore;

    this.setDesc = '';
    this.selectedSetIndex = 0;
  }

  resetCurrent() {
    let params = {title: 'Confirm Reset', body: 'Are you sure?', buttonOk: 'Yes', buttonCancel: 'No'};
    this.dialogService.open({ viewModel: ConfirmDialog, model: params}).then((result) => {
      if (!result.wasCancelled) {
        this.dispatcher.dispatch("current.reset", undefined)
        this.dispatcher.dispatch('save-set.saveCurrent', undefined);

        let msg = new MessageItem(
            MessageType.TypeEnum.Success,
            "Current Set",
            "Reset completed!");
        this.dispatcher.dispatch('msg.post', msg);
      }
    });
  }

  emailSettings() {
    let data = this.store.getCurrentSetStringData();
    let link = "<a href='http://xcell.theflighthangar.com/#/loademailset?set=" + data + "'>Website Link</a>";
    let body = "Note!<br>This will overwrite anything in the current working set.<br>Save that into a new set if you wish to save it.<br>" + link + "&subject=Flight Info - " + this.aircraftData.availableAircraft[this.aircraftData.selectedAircraft].title;
    let url = "mailto:?body=" + body;

    window.location = url;
  }

  loadSettings() {
    if(this.selectedSetIndex < 0) {
      let msg = new MessageItem(MessageType.TypeEnum.Error, "Load Set", "No set selected or available!");
      this.dispatcher.dispatch('msg.post', msg);
    }

    this.store.loadSet(this.selectedSetIndex);
  }

  deleteSettings() {
    if(this.selectedSetIndex < 0) {
      let msg = new MessageItem(MessageType.TypeEnum.Error, "Delete Set", "No set selected or available!");
      this.dispatcher.dispatch('msg.post', msg);

      return;
    }

    let params = {title: 'Confirm Delete', body: 'Are you sure?', buttonOk: 'Yes', buttonCancel: 'No'};
    this.dialogService.open({ viewModel: ConfirmDialog, model: params}).then((result) => {
      if (!result.wasCancelled) {
        this.store.deleteSet(this.selectedSetIndex);
      }
    });
  }

  createSettings() {
    if(this.store.sets.length >= SaveSetStore.setMax) {
      let msg = new MessageItem(MessageType.TypeEnum.Error, "Limit", "Only " + SaveSetStore.setMax + " save sets are allowed!");
      this.dispatcher.dispatch('msg.post', msg);

      return;
    }

    var params = {setName:"", maxChars: SaveSetStore.maxNameLength};
    this.dialogService.open({ viewModel: SaveSetDialog, model: params}).then((result) => {
      console.log("result.wasCancelled: " + result.wasCancelled)
      if (!result.wasCancelled) {
         this.store.saveSet(params.setName);

        let msg = new MessageItem(
            MessageType.TypeEnum.Success,
            "Create Set",
            params.setName + " saved!");
        this.dispatcher.dispatch('msg.post', msg);

      }
    });
  }
}


