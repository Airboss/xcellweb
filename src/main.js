import 'bootstrap';

export function configure(aurelia) {
  aurelia.use
      .standardConfiguration()
      .developmentLogging()
      .globalResources('resources/globals.js')
      .plugin('aurelia-dialog')
      .plugin('aurelia-flux');

  aurelia.start().then(() => aurelia.setRoot());
}
