import {Dispatcher, handle} from 'aurelia-flux';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store.js';

export class Performance {
  static inject = [Dispatcher, AircraftDataStore];

  constructor(dispatcher, aircraftData) {
    this.dispatcher = dispatcher;
    this.aircraft = aircraftData.availableAircraft[aircraftData.selectedAircraft];
  }

  deactivate() {
    this.dispatcher.dispatch('currSet.save', undefined);
  }

  addItem() {
    this.dispatcher.dispatch('perf.addRow', undefined)
  }

  removeItem() {
    this.dispatcher.dispatch('perf.removeRow', undefined)
  }
}