import {Dispatcher, handle} from 'aurelia-flux';
import {MessageItem, MessageType} from 'messages/message';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store';
import {MultiObserver} from 'resources/multi-observer';
import {computedFrom} from 'aurelia-framework';

export class PerfStore {
  static inject = [MultiObserver, AircraftDataStore, Dispatcher];

  constructor(multiObserver, aircraftData, dispatcher) {
    this.multiObserver = multiObserver;
    this.aircraftData = aircraftData;
    this.dispatcher = dispatcher;

    this.resetCurrent();
  }

  @handle('current.reset')
  resetCurrent() {
    this.selectedIndex = 0;
    this.items = [];
  }

  toJSON() {
    let array = [];
    for(var i=0; i < this.items.length; i++){
      let a = [
        this.items[i].icao,
        this.items[i].elev,
        this.items[i].baroHg,
        this.items[i].highTemp,
        this.items[i].toDist,
        this.items[i].toDist50,
        this.items[i].ldDist,
        this.items[i].ldDist50
      ];
      array.push(a);
    }

    let data = JSON.stringify(array);
    return data;
  }

  fromJSON(jsonData) {
    //clear for new data
    if(this.items.length > 0)
      this.items = [];

    let arrays = JSON.parse(jsonData);
    let item = {};

    for (var i = 0; i < arrays.length; i++)
    {
      var data = arrays[i];

      if( data.length !== 8)
        continue;

      let options = {
        icao: data[0],
        elev: data[1],
        baroHg: data[2],
        highTemp: data[3],
        toDist: data[4],
        toDist50: data[5],
        ldDist: data[6],
        ldDist50: data[7]
      };

      item = new PerfItem(this.multiObserver, this.dispatcher, options);
      this.items.push(item);
    }
  }

  @handle('config.aircraft')
  doSetAircraft(action, param) {
    this.setAircraft(param);
  }

  setAircraft(aircraft) {
    this.aircraft = aircraft;
  }

  @handle('perf.removeRow')
  removeRow(action, param) {
    if(this.items.length > 0) {
      this.items.pop();

      this.dispatcher.dispatch('perf.update', undefined);
    }
  }

  @handle('perf.addRow')
  doAddRow(action, param) {
    this.addRow();
  }

  addRow(options) {
    if(this.items.length > 4) {
      let msg = new MessageItem(MessageType.TypeEnum.Error, "Performance", "Only 5 legs can be entered at any time!");
      this.dispatcher.dispatch('msg.post', msg);
      return;
    }

    let newItem = new PerfItem(this.multiObserver, this.dispatcher, options);
    this.items.push(newItem);
  }
}

export class PerfItem {
  stdHg = 29.92;
  stdTemp = 15;

  constructor(multiObserver, dispatcher, options){
    this.multiObserver = multiObserver;
    this.dispatcher = dispatcher;
    this.setDefault(options);

    this.multiObserver.observe(
        [[this, 'icao'], [this, 'elev'], [this, 'baroHg'], [this, 'highTemp']],
        ()=>{this.doCalc();}
    );
  }

  setDefault(options) {
    options = options || {icao: "", elev: "", highTemp: "", baroHg: "", toDist: 0, toDist50: 0, ldDist: 0, ldDist50: 0};

    this.icao = options.icao;
    this.elev = options.elev;
    this.highTemp = options.highTemp;
    this.baroHg = options.baroHg;
    this.toDist = options.toDist;
    this.toDist50 = options.toDist50;
    this.ldDist = options.ldDist;
    this.ldDist50 = options.ldDist50;

    this.doCalc()
  }

  doCalc() {
    this.calcPresAlt();
    this.densityAlt = this.calcDensityAlt();
    this.dispatcher.dispatch('perfChart.update', undefined);
  }

  calcPresAlt(){
    this.presAlt = parseInt(this.elev) + ~~((this.stdHg-this.baroHg)*1000);
    this.calcStdTemp();
  }

  calcStdTemp(){
    var diff = (~~(this.presAlt/1000)) * 2;
    var fractional = ((this.presAlt%1000)+1000)%1000;

    if(fractional > 249 && fractional < 750)
      diff += 1;
    else if(fractional > 749)
      diff += 2;

    this.stdTemp = 15 - diff;

    return this.stdTemp;
  }

  calcDensityAlt() {
    return ~~(this.presAlt + (120.0 * (this.highTemp-this.stdTemp) + .5));
  }

  set densityAlt(value) {
    this._densityAlt = value;
  }

  get densityAlt(){
    return this._densityAlt;
  }
}


