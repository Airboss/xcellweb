import {DialogController} from 'aurelia-dialog';
import {DialogService} from 'aurelia-dialog';
import {MaskedPadDialog} from 'resources/dialogs/masked-pad-dialog'

export class PerfEditDialog {
  static inject = [DialogController, DialogService];

  constructor(controller, dialogService) {
    this.controller = controller;
    this.dialogService = dialogService;
    this.controller.settings.lock = true;
    this.controller.settings.centerHorizontalOnly = true;
  }

  activate(params) {
    this.params = params;
    this.title = params.title;
    this.aircraft = params.aircraft;
    this.item = params.item;
  }

  close() {
    this.controller.ok();
  }

  openMaskedPad(config) {
    if(config == undefined)
      return;

    this.dialogService.open({ viewModel: MaskedPadDialog, model: config}).then((result) => {});
  }
}