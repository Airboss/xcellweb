import {Dispatcher, handle} from 'aurelia-flux';
import {customElement} from 'aurelia-framework';
import {MultiObserver} from 'resources/multi-observer';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store';
import {PerfStore} from './perf.store';

@customElement("perf-chart")
export class PerfChart {
  static inject = [MultiObserver, AircraftDataStore, PerfStore, Dispatcher];

  heading = 'Performance';

  constructor(multiObserver, aircraftData, perfStore, dispatcher) {
    this.multiObserver = multiObserver;
    this.aircraftData = aircraftData;
    this.store = perfStore;
    this.dispatcher = dispatcher;

    this.distRange = 0;
    this.isTakeoff = true;

    this.setChartValues();
  }

  attached() {
    if(this.store.items.length == 0)
      return;

    this.setChartValues();
    this.xAxisLeft = this.calcXAxisLeft(0);
    let base = this.store.items[this.store.selectedIndex].densityAlt;
    if(base < 0)
      base = 0;

    this.yAxisTop = this.calcYAxisTop(base);
  }

  updateXAxisLeft() {
    this.xAxisLeft = this.calcXAxisLeft(this.distRange);
  }

  calcXAxisLeft(value) {
    let v = parseInt(value);
    this.chartDist = Math.round(this.xRngStart + (v * this.distScalar));
    return this.xOffset + v;
  }

  @handle('perfChart.update')
  updateYAxisTop() {
    this.yAxisTop = this.calcYAxisTop(this.store.items[this.store.selectedIndex].densityAlt);
  }

  calcYAxisTop(value) {
    return this.yOffset - (parseInt(value)/this.altScalar);
  }

  setTakeoffChart() {
    this.isTakeoff = true;
    this.setChartValues();
  }

  setLandingChart() {
    this.isTakeoff = false;
    this.setChartValues();
  }

  setChartValues() {
    let aircraft = this.aircraftData.availableAircraft[this.aircraftData.selectedAircraft];

    this.takeoffImage = aircraft.takeoffDistData[0];
    this.landingImage = aircraft.landingDistData[0];

    if(this.isTakeoff) {
      this.xOffset = aircraft.takeoffDistData[1];
      this.xLimit = aircraft.takeoffDistData[2];
      this.yOffset = aircraft.takeoffDistData[3];
      this.yLimit = aircraft.takeoffDistData[4];
      this.xRngStart = aircraft.takeoffDistData[5];
      this.xRngEnd = aircraft.takeoffDistData[6];
      this.yRngStart = aircraft.takeoffDistData[7];
      this.yRngEnd = aircraft.takeoffDistData[8];
      this.distScalar = (this.xRngEnd-this.xRngStart) / this.xLimit;
      this.altScalar = (this.yRngEnd-this.yRngStart) / this.yLimit;
    }
    else {
      this.xOffset = aircraft.landingDistData[1];
      this.xLimit = aircraft.landingDistData[2];
      this.yOffset = aircraft.landingDistData[3];
      this.yLimit = aircraft.landingDistData[4];
      this.xRngStart = aircraft.landingDistData[5];
      this.xRngEnd = aircraft.landingDistData[6];
      this.yRngStart = aircraft.landingDistData[7];
      this.yRngEnd = aircraft.landingDistData[8];
      this.distScalar = (this.xRngEnd-this.xRngStart) / this.xLimit;
      this.altScalar = (this.yRngEnd-this.yRngStart) / this.yLimit;
    }
    this.updateYAxisTop();
  }

  updateTODist() {
    let item = this.store.items[this.store.selectedIndex];
    item.toDist = this.chartDist;
  }

  updateTODist50() {
    this.store.items[this.store.selectedIndex].toDist50 = this.chartDist;
  }

  updateLndDist() {
    this.store.items[this.store.selectedIndex].ldDist = this.chartDist;
  }

  updateLndDist50() {
    this.store.items[this.store.selectedIndex].ldDist50 = this.chartDist;
  }
}
