import {customElement, inject, bindable} from 'aurelia-framework';
import {DialogService} from 'aurelia-dialog';
import {PerfStore} from './perf.store';
import {PerfEditDialog} from './perf-edit-dialog';

@customElement("perf")
export class Perf {
  static inject = [DialogService, PerfStore];

  constructor(dialogService, perfStore) {
    this.dialogService = dialogService;
    this.store = perfStore;

    this.item = {};
    this.showing = false;
  }

  editItem(index) {
    this.store.selectedIndex = index;

    let params = {title: "Leg " + (index+1), aircraft: this.store.aircraft, item: this.store.items[index]};
    this.dialogService.open({viewModel: PerfEditDialog, model: params});
  }
}
