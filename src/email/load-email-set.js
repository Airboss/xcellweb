import {Router} from 'aurelia-router';
import {Dispatcher} from 'aurelia-flux';
import {MessageItem, MessageType} from 'messages/message';
import {SaveSetStore} from 'save-set/save-set.store.js';

export class LoadEmailSet {
  static inject = [Router, Dispatcher, SaveSetStore];

  constructor(router, dispatcher, saveSetStore)  {
    this.theRouter = router;
    this.dispatcher = dispatcher;
    this.store = saveSetStore;
  }

  activate(params, routeConfig)
  {
    if (params.set !== undefined)
    {
      let msg = new MessageItem(MessageType.TypeEnum.Success, "Email Set", "Email Set found! Processing...");
      this.dispatcher.dispatch('msg.post', msg);
      this.store.loadEmailSet(params.set);
    }
    this.theRouter.navigate("");
  }
}