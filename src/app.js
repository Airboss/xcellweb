import 'bootstrap';
import 'bootstrap/css/bootstrap.css!';
import {Dispatcher} from 'aurelia-flux';
import {AircraftDataStore} from 'aircraft-data/aircraft-data.store';
import {MessageService} from 'messages/message-service';
import {MessageItem, MessageType} from 'messages/message';
import {SaveSetStore} from 'save-set/save-set.store';

export class App {
  // note: aircraftData, messageService injected here by DI so initialized
  // and available to rest of application
  // Add any additional dependancies before AircraftDataStore.
  static inject = [Dispatcher, SaveSetStore, AircraftDataStore, MessageService];

  constructor(dispatcher, saveSetStore, aircraftData, messageService) {
    this.dispatcher = dispatcher;
    this.saveSetStore = saveSetStore;
  }

  attached(config, queryString, routeConfig) {
    // Needs to be here or toastr message not seen.
    // Load current set on start up
    this.dispatcher.dispatch("currSet.load", undefined);
  }

  configureRouter(config, router){
    config.title = 'X-Cell';
    config.map([
      { route: ['','home'], moduleId: 'home/home', nav: false, title:'Home' },
      { route: 'loademailset', moduleId: 'email/load-email-set', nav: false, title:'Load Email Set' },
      { route: 'weightbalance', moduleId: 'wt-bal/weight-balance', nav: true, title:'Weight & Balance' },
      { route: 'performance', moduleId: 'perf/performance', nav: true, title:'Performance' },
      { route: 'flightlog', moduleId: 'flight-log/flight-log', nav: true, title:'Flight Log' }
    ]);

    this.router = router;
  }
}
