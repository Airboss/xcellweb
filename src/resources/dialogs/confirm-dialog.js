import {DialogController} from 'aurelia-dialog';

export class ConfirmDialog {
  static inject = [DialogController];

  constructor(controller){
    this.controller = controller;
  }

  activate(params){
    this.params = params;
  }
}
