import {DialogController} from 'aurelia-dialog';

export class MaskedPadDialog {
  static inject = [DialogController];

  constructor(controller) {
    this.controller = controller;
    this.controller.settings.lock = true;
    this.controller.settings.centerHorizontalOnly = true;

    this.canSave = false;
  }

  activate(params) {
    this.params = params;
    this.title = params.title;
    this.mask = params.mask;
    this.plusMinus = params.minusAllowed | false;
    this.float = params.floatAllowed | false;
    this.blank = params.blankAllowed | false;

    this.currValue = params.object[this.params.propName];
    if(params.filterIn != undefined) {
      this.currValue = params.filterIn(this.currValue);
    }

    if(this.currValue == undefined) {
      this.currValue = "";
    }

    this.currValue = "" + this.currValue;

    let firstChar = this.currValue.substr(0,1);
    if(this.plusMinus) {
      if(firstChar == "-") {
        this.sign = "-";
      }
      else {
        this.sign = "+";
      }
      this.mask = this.sign + this.mask;
      if(this.sign == "-")
        this.currValue = this.currValue.substr(1, this.currValue.length);
    }

    this.entry = this.setMaskedEntry(this.currValue);
    this.checkSave();

  }

  attached() {
    this.cancelButton.focus();
  }

  ok() {
    if(this.sign == "-")
      this.currValue = this.sign + this.currValue;

    if(this.params.filterOut != undefined) {
      this.currValue = this.params.filterOut(this.currValue);
    }
    else if(this.params.floatAllowed) {
      this.currValue = parseFloat(this.currValue);
    }
    else {
      this.currValue = parseInt(this.currValue);
    }

    this.params.object[this.params.propName] = this.currValue;
    this.controller.ok();
  }

  setMaskedEntry(value) {
    let sa = '' + value;
    if(sa.length > 1)
      sa = value.split("");

    let mask = this.mask.split("");
    for (let i = 0, mi = 0; i < sa.length; i++, mi++) {

      while (mask[mi] != "_") {
        mi++
      }

      mask[mi] = sa[i];
    }

    return mask.join("");
  }

  numPadClick(event) {
    let el = event.target;
    let v = el.dataset.value;

    if (v.toLowerCase() === "c") {
      this.currValue = "";
    }
    else if (v === "-") {
      if(this.sign === "-")
        this.sign = "+";
      else
        this.sign = "-";

      this.mask = this.sign + this.params.mask;
    }
    else if (v.toLowerCase() === "b") {
      if (this.currValue.length <= 0) {
        return;
      }

      let len = this.currValue.length - 1;
      let str = this.currValue.substring(0, len);
        this.currValue = str;
    }
    else if(v === ".") {
        if(this.currValue == undefined)
          this.currValue = ".";
        else if(this.currValue.indexOf(".") != -1)
          return;
        else {
          if (this.currValue.length < this.params.maxDigits)
            this.currValue = this.currValue + v;
        }

    }
    else {
      if (this.currValue == undefined) {
        this.currValue = v;
      }
      else {
        if (this.currValue.length < this.params.maxDigits) {
          this.currValue = this.currValue + v;
        }
      }
    }
    this.entry = this.setMaskedEntry(this.currValue);
    this.checkSave();
  }

  checkSave() {
    if (this.currValue != undefined
        && ((this.currValue.length == 0 && this.blank) || this.currValue.length >= this.params.minDigits)) {
      this.canSave = true;
    }
    else {
      this.canSave = false;
    }
  }
}

export class formatHHMMValueConverter {
  toView(value) {
    if (value === undefined || value === "") {
      return value;
    }

    let hh = value.slice(0, 2);
    let mm = value.slice(2, 4);

    return hh + ":" + mm;
  }
}