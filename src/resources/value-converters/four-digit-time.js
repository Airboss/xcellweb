export class FourDigitTimeValueConverter {
  fromView(value) {
    return value;
  }

  toView(value) {
    if(value === undefined || value === "")
      return value;

    return value.padZero(4);
  }
}