export class EastWestToIntValueConverter {
  toView(value){
    return value;
  }

  fromView(value){
    var idx = value.toUpperCase().indexOf("E");
    if( idx != -1)
    {
      var sval = value.substr(0,idx);
      return parseInt(-sval);
    }

    idx = value.toUpperCase().indexOf("W");
    if( idx != -1)
    {
      var sval = value.substr(0,idx);
      return parseInt(+sval);
    }

    return value;
  }
}
