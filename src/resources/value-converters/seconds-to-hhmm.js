export class SecondsToHhmmValueConverter {
  toView(value){
    if(value === undefined || value === "")
      return value;

    let hours   = Math.floor(value / 3600);
    let minutes = Math.floor((value - (hours * 3600)) / 60);

    let v = hours.padZero(2) + ":" + minutes.padZero(2);
    return v;
  }
}
