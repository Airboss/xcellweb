export class padNumberValueConverter {
  toView(value, numDigits){
    if(value == undefined || value == "")
      return value;

    return value.padZero(numDigits);
  }
}
