export class UppercaseValueConverter {
  toView(value){
    if(value === undefined || value === "")
      return value;

    return value.toUpperCase();
  }
  fromView(value) {
    if(value === undefined || value === "")
      return value;

    return value.toUpperCase();
  }
}