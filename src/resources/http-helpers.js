import {HttpClient} from 'aurelia-http-client';

export class HttpHelpers {
  static inject = [HttpClient];
  dataPromise = null;

  constructor(http) {
    this.http = http;
  }

  getJSONData(url, nocache) {
    nocache = nocache || false;
    if (!this.dataPromise) {
      this.dataPromise = this.http.get( url + (nocache ? '?nocache=' + (new Date()).getTime() : ''));
    }
    return this.dataPromise;
  }
}

