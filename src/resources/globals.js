//Example usage var v=2; v.padZero(4); v.padZero(4,'+'); produces '0002' '+++2'
String.prototype.padZero= function(len, c){
  var s= '', c= c || '0', len= (len || 2)-this.length;
  while(s.length<len) s+= c;
  return s+this;
}
Number.prototype.padZero= function(len, c){
  return String(this).padZero(len,c);
}