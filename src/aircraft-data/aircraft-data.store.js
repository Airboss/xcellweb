import {Dispatcher, handle} from 'aurelia-flux';
import {MessageItem, MessageType} from 'messages/message';

export class AircraftDataStore {
  static inject = [Dispatcher];

  constructor(dispatcher) {
    this.dispatcher = dispatcher;

    this._selectedAircraft = 0;
    this.availableAircraft =
        [
          {
            "reg": "N601FL",
            "title": "N601FL - Pa28-140 - Piper Cherokee",
            "takeoffDistData": ["/img/Pa28-140-TakeOff.png", 56, 299, 388, 256, 500, 4000, 0, 7000],
            "landingDistData": ["/img/Pa28-140-Landing.png", 56, 294, 392, 258, 0, 1400, 0, 7000],
            "cgData": ["/img/Pa28-140-CgChart.png", 34, 539, 473, 450, 84, 96, 1200, 2200],
            "planeEmptyWeight": 1276,
            "planeGrossWeight": 2150,
            "planeEmptyArm": 84.73,
            "planeCgRange": [84,96],
            "pilotCopilotArm": 85.5,
            "rearPassengersArm": 117,
            "rearBaggageArm": 117,
            "fuelArm": 95,
            "stdCruise": 105,
            "fuelCapacity": 50,
            "fuelGph": 10,
            "stdFuelClimb": 1
          },
          {
            "reg": "N8182N",
            "title": "N8182N - Pa28-140 - Piper Cherokee",
            "takeoffDistData": ["/img/Pa28-140-TakeOff.png", 56, 299, 388, 256, 500, 4000, 0, 7000],
            "landingDistData": ["/img/Pa28-140-Landing.png", 56, 294, 392, 258, 0, 1400, 0, 7000],
            "cgData": ["/img/Pa28-140-CgChart.png", 34, 539, 473, 450, 84, 96, 1200, 2200],
            "planeEmptyWeight": 1316,
            "planeGrossWeight": 2150,
            "planeEmptyArm": 87.16,
            "planeCgRange": [84,96],
            "pilotCopilotArm": 85.5,
            "rearPassengersArm": 117,
            "rearBaggageArm": 125,
            "fuelArm": 95,
            "stdCruise": 105,
            "fuelCapacity": 50,
            "fuelGph": 10,
            "stdFuelClimb": 1
          },
          {
            "reg": "N6950W",
            "title": "N6950W - Pa28-140 - Piper Cherokee",
            "takeoffDistData": ["/img/Pa28-140-TakeOff.png", 56, 299, 388, 256, 500, 4000, 0, 7000],
            "landingDistData": ["/img/Pa28-140-Landing.png", 56, 294, 392, 258, 0, 1400, 0, 7000],
            "cgData": ["/img/Pa28-140-CgChart.png", 34, 539, 473, 450, 84, 96, 1200, 2200],
            "planeEmptyWeight": 1274,
            "planeGrossWeight": 2150,
            "planeEmptyArm": 87.5,
            "planeCgRange": [84,96],
            "pilotCopilotArm": 85.5,
            "rearPassengersArm": 117,
            "rearBaggageArm": 125,
            "fuelArm": 95,
            "stdCruise": 105,
            "fuelCapacity": 50,
            "fuelGph": 10,
            "stdFuelClimb": 1
          },
          {
            "reg": "N4967T",
            "title": "N4967T - Pa28-200R - Piper Arrow",
            "takeoffDistData": ["/img/Pa28R-200-TakeOff.png", 52, 310, 528, 497, 0, 5000, 0, 8000 ],
            "landingDistData": ["/img/Pa28R-200-Landing.png", 52, 310, 519, 495, 0, 2000, 0, 8000],
            "cgData": ["/img/Pa28R-200-CgChart.png", 29, 585, 607, 584, 80, 93, 1400, 2700],
            "planeEmptyWeight": 1649,
            "planeGrossWeight": 2650,
            "planeEmptyArm": 83.5,
            "planeCgRange": [80,93],
            "pilotCopilotArm": 80.5,
            "rearPassengersArm": 118.1,
            "rearBaggageArm": 142.8,
            "fuelArm": 95,
            "stdCruise": 135,
            "fuelCapacity": 50,
            "fuelGph": 12,
            "stdFuelClimb": 2
          }
        ];
  }

  get selectedAircraft() {
    return this._selectedAircraft;
  }

  set selectedAircraft(value) {
    if(this._selectedAircraft === value)
      return;

    this._selectedAircraft = value;

    let msg = new MessageItem(MessageType.TypeEnum.Success, "Aircraft Changed", "Aircraft changed to " + this.availableAircraft[this.selectedAircraft].title);
    this.dispatcher.dispatch('msg.post', msg);
    this.dispatcher.dispatch('config.aircraft', this.availableAircraft[this.selectedAircraft]);
  }

  updateAircraftByReg(reg) {
    console.log("aircraft-data.store_updateAircraftByReg");

    for(var i = 0; i < this.availableAircraft.length; i++) {
      if(reg === this.availableAircraft[i].reg)
        this.selectedAircraft = i;
    }

    this.dispatcher.dispatch('config.aircraft', this.availableAircraft[this.selectedAircraft]);
  }
}
